import { User } from "../user";

export interface LoginRequest {
    login: string;
    password: string;
}

export interface LoginResult { 
    userData: User;
    token: string;
}