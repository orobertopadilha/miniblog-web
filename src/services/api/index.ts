import axios from "axios"

const createApi = () => { 
    return axios.create({ 
        baseURL: 'http://localhost:8080/'
    })
}

const api = createApi()

export default api