import { LoginRequest, LoginResult } from "../../../model/auth";
import api from "../../api";

export const postLogin = async (loginData: LoginRequest): Promise<LoginResult> => {
    let result = await api.post<LoginResult>('/miniblog-users-api/login', loginData)
    return result.data
}