import { useState } from "react"
import { loginService } from "../../../../services/auth"
import { TUseLogin } from "./interfaces"

const useLogin = (): TUseLogin => { 
    const [login, setLogin] = useState('')
    const [password, setPassword] = useState('')

    const executeLogin = async () => {
        let result = await loginService.postLogin({ login, password })
        console.log(result)
    }

    return { 
        setLogin,
        setPassword,
        executeLogin
    }
}

export default useLogin