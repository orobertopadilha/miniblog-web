export interface TUseLogin {
    setLogin: (login: string) => void,
    setPassword: (password: string) => void,
    executeLogin: () => void
}