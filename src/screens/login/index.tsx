import useLogin from "./hooks/useLogin";

const Login = () => { 
    const { setLogin, setPassword, executeLogin } = useLogin()

    return (
        <>
           <input type='text' placeholder='login' 
                onChange={e => setLogin(e.target.value)}/> <br />
           <input type='password' placeholder='password' 
                onChange={e => setPassword(e.target.value) }/> <br />
           <button onClick={executeLogin}>Login!</button>
        </>
    )
}

export default Login;