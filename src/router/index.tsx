import { Route, Routes } from 'react-router-dom';
import Home from '../screens/home';
import Login from '../screens/login';

const AppRouter = () => { 
    return (
        <Routes>
            <Route path="/" element={<Login />}/>
            <Route path="/home" element={<Home />}/>
        </Routes>
    )
}

export default AppRouter;